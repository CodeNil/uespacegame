startfilelines = open('phoebe_ver64q.tab', 'r').readlines()

cur_num = 0
on_faces = False

outputstring = ''

for i, line in enumerate(startfilelines):
	if i % 5000 == 0:
		print(i / len(startfilelines))
	columns = line.strip().replace('      ', ' ').replace('   ', ' ').replace('   ', ' ').replace('  ', ' ').split(' ')
	if len(columns) == 4:
		if int(columns[0]) < cur_num:
			on_faces = True
		cur_num = int(columns[0])
		if on_faces:
			outputstring += 'f ' + columns[1] + ' ' + columns[2] + ' ' + columns[3] + '\n'
		else:
			outputstring += 'v ' + columns[1] + ' ' + columns[2] + ' ' + columns[3] + '\n'

savefile = open('output.obj', 'w')
savefile.write(outputstring)
savefile.close()
